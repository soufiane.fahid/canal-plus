const jsonServer = require("json-server");
const server = jsonServer.create();
const router = jsonServer.router("data.json");
const middleware = jsonServer.defaults();
const routes = require("./routes.json");

server.use(jsonServer.rewriter(routes));
server.use(middleware);
server.use(jsonServer.bodyParser);


server.use(router);
router.render = (req, res) => {
  if (req.method === 'PUT' || req.method === 'OPTIONS' ) {
    res.status(200).jsonp({});
  } else {
    const data = res.locals.data.filter((elem, index) => {
      return index < 100;
    });
    res.jsonp(data);
  }
}

server.listen(3000, () => {
  console.log("JSON Server is running");
})