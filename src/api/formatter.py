#!/usr/bin/python
"""
Read TSV from stdin and output as JSON.
"""

import csv
import json

csvfile = open('data.tsv', 'r')
jsonfile = open('new_data.json', 'w')

fieldnames = ("tconst","titleType","primaryTitle","originalTitle","isAdult","startYear","endYear","runtimeMinutes","genres")
reader = csv.DictReader( csvfile, fieldnames)
for row in reader:
    json.dump(row, jsonfile)
    jsonfile.write('\n')
