import { Component } from '@angular/core';

@Component({
  selector: 'mc-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.scss'],
})

export class MCSpinner {}