import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { MCSpinner } from './spinner/spinner.component';

@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    RouterModule,
  ],
  declarations: [
    MCSpinner,
  ],
  exports: [
    MCSpinner,
    CommonModule,
  ],
  providers: [],
})

export class SharedModule {}
