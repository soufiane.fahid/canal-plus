import { Component, Input } from '@angular/core';
import { Media } from '@app/core/models';
import { Store } from '@ngrx/store';
import * as fromStore from '@app/core/store';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { MediaComponent } from '../media/media.component';

@Component({
  selector: 'mc-medias-list',
  templateUrl: './medias-list.component.html',
  styleUrls: ['./medias-list.component.scss'],
})

export class MediasListComponent {

  @Input() medias: Media[] = [];
  popin: NgbModalRef;


  constructor(
    private store: Store<fromStore.AppState>,
    private modalService: NgbModal,
  ) {}

  updateMedia(media: Media) {
    this.popin = this.modalService.open(MediaComponent, {
      centered: true,
      backdrop: 'static',
      keyboard: true,
    })
    this.popin.componentInstance.media = media;
    this.store.dispatch(new fromStore.SelectedMedia(media));
  }

}