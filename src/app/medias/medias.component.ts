import { Component, OnInit, OnDestroy } from '@angular/core';
import { select, Store } from '@ngrx/store';
import * as fromStore from '@app/core/store';
import { Subscription } from 'rxjs';
import { Media } from '@app/core/models';

@Component({
  selector: 'mc-medias',
  templateUrl: './medias.component.html',
  styleUrls: ['./medias.component.scss'],
})

export class MediasComponent implements OnInit, OnDestroy {

  subscription$: Subscription = new Subscription();
  medias: Media[] = [];
  isMediasLoading: boolean = true;


  constructor(
    private store: Store<fromStore.AppState>,
  ) {}

  ngOnInit() {
    this.subscription$.add(
      this.store.pipe(select(fromStore.getMedias)).subscribe((medias) => {
        this.medias = medias;
      }),
    );

    this.subscription$.add(
      this.store.pipe(select(fromStore.isMediasLoading)).subscribe(isMediasLoading =>  this.isMediasLoading = isMediasLoading),
    );
  }

  ngOnDestroy() {
    this.subscription$.unsubscribe();
  }

}