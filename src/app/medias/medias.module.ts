import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MediasGuard } from '@app/core/guards';

import { MediasRoutingModule } from '@app/medias/medias-routing.module';
import { MediasComponent } from '@app/medias/medias.component';
import { MediasListComponent } from '@app/medias/medias-list/medias-list.component';
import { MediasHeaderComponent } from '@app/medias/medias-header/medias-header.component';
import { MediaComponent } from '@app/medias/media/media.component';
import { SharedModule } from '@app/shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    MediasRoutingModule,
    NgbModule,
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
  ],
  declarations: [
    MediasComponent,
    MediasListComponent,
    MediasHeaderComponent,
    MediaComponent,
  ],
  entryComponents: [
    MediaComponent,
  ],
  exports: [
    MediasComponent,
  ],
  providers: [
    MediasGuard,
  ]
})

export class MediasModule {}