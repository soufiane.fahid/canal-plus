import { Component, Input, Output, OnInit, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import * as fromStore from '@app/core/store';
import { Subscription } from 'rxjs';

@Component({
  selector: 'mc-medias-header',
  templateUrl: './medias-header.component.html',
  styleUrls: ['./medias-header.component.scss'],
})

export class MediasHeaderComponent implements OnInit, OnDestroy {

  query: string;
  subscription$: Subscription = new Subscription();
  dateStringHasError: boolean = false;

  constructor(
    private store: Store<fromStore.AppState>,
  ) {}

  ngOnInit() {}

  titleSearchChange(event) {
    const title = event.target.value;

    if (title.length < 3) {
      this.store.dispatch(new fromStore.MediaUpdateQuery(null));
    }

    if (title.length >= 3) {
      this.store.dispatch(new fromStore.MediaUpdateQuery(title));
    }
  }

  dateSearchChange(event) {
    const dateString = event.target.value;

    const isValidDate = dateString.match(/^[0-9]+$/) !== null;


    if (dateString.length !== 4 || !isValidDate) {
      this.dateStringHasError = true;
    }

    if (dateString.length === 4 && isValidDate) {
      this.dateStringHasError = false;
      this.store.dispatch(new fromStore.MediaUpdateDateQuery(dateString));
    }

    if (dateString.length === 0) {
      this.dateStringHasError = false;
      this.store.dispatch(new fromStore.MediaUpdateDateQuery(null));
    }
  }

  ngOnDestroy() {
    this.subscription$.unsubscribe();
  }

}