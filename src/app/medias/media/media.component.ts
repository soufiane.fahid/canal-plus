import { Component, Input, OnInit } from '@angular/core';
import { Media } from '@app/core/models';
import { Store } from '@ngrx/store';
import * as fromStore from '@app/core/store';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl } from '@angular/forms';
import { UpdateMediaRequest } from '@app/core/requests/update.request';

@Component({
  selector: 'mc-media',
  templateUrl: './media.component.html',
  styleUrls: ['./media.component.scss'],
})

export class MediaComponent implements OnInit {

  @Input() media: Media;
  updateMediaForm: FormGroup;

  constructor(
    private store: Store<fromStore.AppState>,
    public activeModal: NgbActiveModal,
  ) {}

  ngOnInit() {
    //TO-ADD the validators (default 'required' & custom validators)
    this.updateMediaForm = new FormGroup({
      primaryTitle: new FormControl(''),
      startYear: new FormControl(''),
      gender: new FormControl(''),
    });
  }

  onSubmit() {
    const updatedMedia: UpdateMediaRequest = {
      primaryTitle: this.primaryTitle,
      startYear: this.startYear,
      genres: this.gender,
      tconst: this.media.tconst,
    }
    this.store.dispatch(new fromStore.UpdateMedia(updatedMedia));
    this.activeModal.dismiss();
  }

  get primaryTitle() {
    return this.updateMediaForm.get('primaryTitle').value;
  }

  get startYear() {
    return this.updateMediaForm.get('startYear').value;
  }

  get gender() {
    return this.updateMediaForm.get('gender').value;
  }

  close() {
    this.activeModal.dismiss();
  }
}