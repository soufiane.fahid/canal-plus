import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Store } from '@ngrx/store';
import { RouterTestingModule } from '@angular/router/testing';
import { MediasComponent } from './medias.component';
import { CoreModule } from '@app/core/core.module';
import * as fromMedias from '@app/core/store/reducers';

describe('MediasComponentTest', () => {
  let component: MediasComponent;
  let fixture: ComponentFixture<MediasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [CoreModule, RouterTestingModule],
      declarations: [
        MediasComponent,
      ],
      providers: [
        Store,
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(MediasComponent);
    component = fixture.componentInstance;
  })

  it('should Create the component', inject(
    [Store],
    (store: Store<fromMedias.AppState>) => {
      expect(component).toBeTruthy();
    }
  ))

})
