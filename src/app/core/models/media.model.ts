import { JsonObject, JsonProperty } from 'json2typescript';

@JsonObject()
export class Media {

  @JsonProperty('tconst', String, true)
  tconst?: string = null;

  @JsonProperty('titleType', String, true)
  titleType?: string = null;

  @JsonProperty('primaryTitle', String, true)
  primaryTitle?: string = null;

  @JsonProperty('originalTitle', String, true)
  originalTitle?: string = null;

  @JsonProperty('isAdult', String, true)
  isAdult?: string = null;

  @JsonProperty('startYear', String, true)
  startYear?: string = null;

  @JsonProperty('endYear', String, true)
  endYear?: string = null;

  @JsonProperty('runtimeMinutes', String, true)
  runtimeMinutes?: string = null;

  @JsonProperty('genres', String, true)
  genres?: string = null;

  isMediaForAdult: boolean = false;

  get formattedAdultMedia() {
    return this.isMediaForAdult ? 'Exclusives aux adultes' : 'Tous les âges';
  }
}