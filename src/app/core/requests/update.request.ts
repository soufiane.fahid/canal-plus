// Interface used with the media update
export interface UpdateMediaRequest {
  primaryTitle: string;
  startYear: string;
  genres: string;
  tconst: string;
}
