import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { environment } from '@env/environment';

@Injectable()
export class DataService {
  constructor(private http: HttpClient) {}

  private formattedErrors(error: any) {
    return throwError(error);
  }

  //TO Addd the environments
  get(path: string, params: HttpParams = new HttpParams()): Observable<any> {
    return this.http
      .get(`${environment.urlApi}${path}`, {params, withCredentials: true})
      .pipe(catchError(this.formattedErrors));
  }

  put (path: string, body: Object = {}):Observable<any> {
    const headers = new HttpHeaders().set ('content-type', 'applicaion/json; chartset=utf-8');
    return this.http
      .put(`${environment.urlApi}${path}`, JSON.stringify(body), {headers, withCredentials: true})
      .pipe(catchError(this.formattedErrors));
  }

  delete(path): Observable<any> {
    return this.http
      .delete(`${environment.urlApi}${path}`)
      .pipe(catchError(this.formattedErrors));
  }
}
