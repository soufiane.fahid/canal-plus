import { DataService } from './data.service';
import { MediasService } from './medias.service';

export const services: any[] = [
  DataService,
  MediasService,
]

export * from './data.service';
export * from './medias.service';
