import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { JsonConvert } from 'json2typescript';
import { MediasMiddleware } from '../middleware';
import { DataService } from './data.service';
import { map } from 'rxjs/operators';
import { Media } from '../models';

@Injectable()
export class MediasService {

  constructor(
    private dataServices: DataService,
    private mediasMiddleware: MediasMiddleware,
    private jsonConvert: JsonConvert,
  ) {}

  getEntities(): Observable<Media[]> {
    return this.dataServices
      .get('/')
      .pipe(
        map(response => this.mediasMiddleware.isMediasForAdult(response)),
        map((response: any) => {
          return response.map((media) =>{
            try {
              return this.jsonConvert.deserialize(media, Media);
            } catch(e) {
              console.error(e);
            }
          })
      }));
  }

  updateMedia(): Observable<any> {
    return this.dataServices
      .put('/')
      .pipe(
        map((response) => {
          try {
            return response;
          } catch(e) {
            console.error(e);
          }
        })
      );
  }

}