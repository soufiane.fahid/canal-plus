import { Injectable } from '@angular/core';
import { Media } from '../models';

@Injectable()
export class MediasMiddleware {

  constructor() {}

  isMediasForAdult(medias: Media[]) {
    return medias.map(media => {
      return { ...media, isMediaForAdult: media.isAdult !== '0'};
    })
  }
}