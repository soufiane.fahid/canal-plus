import { Action } from '@ngrx/store';
import { Media } from '@app/core/models';
import { UpdateMediaRequest } from '@app/core/requests/update.request';

export enum MediasActionTypes {
  MediasLoad = '[Medias] Load',
  MediasLoadComplete = '[Medias] Load Complete',
  MediasLoadError = '[Medias] Load Error',
  MediaUpdateQuery = '[Medias] Update Query',
  SelectedMedia = '[Media] Selected Media',
  UpdateMedia = '[Media] Update ',
  UpdateMediaComplete = '[Media] Update Complete',
  UpdateMediaError = '[Media] Update Error',
  MediaUpdateDateQuery = '[Medias] Update Date Query',
  MediasClear = '[Medias] Clear',
}

// Action dispatched to load medias
export class MediasLoad implements Action {
  readonly type = MediasActionTypes.MediasLoad;
}

//Action dispatched when the load is Completed
export class MediasLoadComplete implements Action {
  readonly type = MediasActionTypes.MediasLoadComplete;

  constructor(public payload: Media[]) {}
}

//Action dispatched when the user is starting the title search
export class MediaUpdateQuery implements Action {
  readonly type = MediasActionTypes.MediaUpdateQuery;

  constructor(public payload: string) {}
}

// Action dispatched when the user is search by date
export class MediaUpdateDateQuery implements Action {
  readonly type = MediasActionTypes.MediaUpdateDateQuery;

  constructor(public payload: string) {}
}


//Action dispatched to Update the media
export class UpdateMedia implements Action {
  readonly type = MediasActionTypes.UpdateMedia;

  constructor(public payload: UpdateMediaRequest) {}
}

//Action dispatched to select the media when the popin is opened
export class SelectedMedia implements Action {
  readonly type = MediasActionTypes.SelectedMedia;

  constructor(public payload: Media) {}
}

//Action dispatched after the update is complete
export class UpdateMediaComplete implements Action {
  readonly type = MediasActionTypes.UpdateMediaComplete;
}

//Action dispatched when an error occured with the media update
export class UpdateMediaError implements Action {
  readonly type = MediasActionTypes.UpdateMediaError;

  constructor(public payload: any) {}
}

//Action dispatched when an error occured with the media load
export class MediasLoadError implements Action {
  readonly type = MediasActionTypes.MediasLoadError;

  constructor(public payload: any) {}
}

//Action dispatched to clear the Media Reducer
export class MediasClear implements Action {
  readonly type = MediasActionTypes.MediasClear;
}

export type MediasActionsUnion =
  | MediasLoad
  | MediasLoadComplete
  | MediasLoadError
  | MediaUpdateQuery
  | UpdateMedia
  | SelectedMedia
  | UpdateMediaComplete
  | UpdateMediaError
  | MediaUpdateDateQuery
  | MediasClear;