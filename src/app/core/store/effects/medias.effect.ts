import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action, Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';

import {
  MediasActionTypes,
  MediasLoad,
  MediasLoadComplete,
  MediasLoadError,
  UpdateMedia,
  UpdateMediaComplete,
  UpdateMediaError,
} from '@app/core/store/actions';
import { AppState } from '@app/core/store/reducers';
import { switchMap, map, catchError } from 'rxjs/operators';
import { MediasService } from '../../services';
import { Media } from '@app/core/models';

@Injectable()
export class MediasEffects {

  @Effect()
  loadMediasEntities$: Observable<Action> = this.action$.pipe(
    ofType<MediasLoad>(MediasActionTypes.MediasLoad),
    switchMap(() => {
      return this.mediasService
        .getEntities()
        .pipe(
          map((medias: Media[]) => new MediasLoadComplete(medias)),
          catchError(error => of(new MediasLoadError(error)))
        )
    })
  );

  @Effect()
  updateMedia$: Observable<Action> = this.action$.pipe(
    ofType<UpdateMedia>(MediasActionTypes.UpdateMedia),
    switchMap(() => {
      return this.mediasService
        .updateMedia()
        .pipe(
          map((response) =>{
            return new UpdateMediaComplete();
          }),
          catchError((error) => {
            return of(new UpdateMediaError(error));
          })
        )
    })
  );

  constructor(
    private action$: Actions,
    private store: Store<AppState>,
    private mediasService: MediasService,
  ) {}
}