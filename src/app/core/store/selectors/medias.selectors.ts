import { createSelector } from '@ngrx/store';

import * as fromFeature from '../reducers';
import * as fromMedias from '../reducers/medias.reducer';

export const getMedias = createSelector(
  fromFeature.getMediasState,
  (state: fromMedias.MediasState) => {
    //No Search Query
    if ((!state.queryTitle || state.queryTitle === '') && (!state.queryDate || state.queryDate === '')) {
      return state.medias;
    }
    //Search only by using the title query
    if (!state.queryDate || state.queryDate === '') {
      return state.medias.filter((media) => {
        return media.originalTitle.includes(state.queryTitle) || media.primaryTitle.includes(state.queryTitle);
      });
    }
    //Search only by using the date query
    if (!state.queryTitle || state.queryTitle === '') {
      return state.medias.filter((media) => {
        return media.startYear === state.queryDate;
      });
    }
    //Search by date & title
    return state.medias.filter((media) => {
      return media.startYear === state.queryDate && (media.originalTitle.includes(state.queryTitle) || media.primaryTitle.includes(state.queryTitle));
    });
  },
)

export const getMediasLoadStatus = createSelector(
  fromFeature.getMediasState,
  (state: fromMedias.MediasState) => {
    let status = 'initial';
    if (state.isLoaded) {
      status = 'success';
    }
    if (!!state.error) {
      status = 'fail';
    }
    return status;
  }
)

export const isMediasLoading = createSelector(
  fromFeature.getMediasState,
  (state: fromMedias.MediasState) => state.isLoading,
)

export const getMediaSearchQuery = createSelector(
  fromFeature.getMediasState,
  (state: fromMedias.MediasState) => state.queryTitle,
)
