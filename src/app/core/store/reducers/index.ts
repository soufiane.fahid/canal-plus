import { routerReducer, RouterReducerState, RouterStateSerializer } from '@ngrx/router-store';
import { ActionReducer, ActionReducerMap, createFeatureSelector, MetaReducer } from '@ngrx/store';
import { storeFreeze } from 'ngrx-store-freeze';
import { Data, Params, RouterStateSnapshot } from '@angular/router';
import { environment } from '@env/environment';
import * as fromMedias from './medias.reducer';

export interface RouterStateURL {
  url: string;
  params: Params;
  queryParams: Params;
  data: Data;
}

export interface AppState {
  router: RouterReducerState<RouterStateURL>;
  medias: fromMedias.MediasState;
}

export const appReducer: ActionReducerMap<AppState> = {
  router: routerReducer,
  medias: fromMedias.reducer,
}

export const getRouterState = createFeatureSelector<RouterReducerState<RouterStateURL>>('router');
export const getMediasState = createFeatureSelector<fromMedias.MediasState>('medias');

export function logger(reducer: ActionReducer<AppState>): ActionReducer<AppState> {
  return function (state: AppState, action: any): AppState {
    return reducer(state, action);
  }
}

export const appMetaReducers: MetaReducer<AppState>[] = !environment.production ? [logger, storeFreeze] : [];

export class CustomSerializer implements RouterStateSerializer<RouterStateURL> {

  serialize(routerState: RouterStateSnapshot): RouterStateURL {
    let route = routerState.root;
    while(route.firstChild) {
      route = route.firstChild;
    }

    const { url } = routerState;
    const queryParams = routerState.root.queryParams;
    const params = route.params;
    const data = route.data;

    return { url, params, queryParams, data };
  }
}
