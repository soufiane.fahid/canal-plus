import { MediasActionsUnion, MediasActionTypes } from '../actions';
import { Media } from '@app/core/models';

export interface MediasState {
  medias: Media[];
  isLoading: boolean;
  isLoaded: boolean;
  queryTitle: string;
  queryDate: string;
  media: Media;
  isUpdating: boolean;
  isUpdated: boolean;
  error: any,
}

export const intialState: MediasState = {
  medias: [],
  isLoading: false,
  isLoaded: false,
  queryTitle: null,
  queryDate: null,
  media: null,
  isUpdating: false,
  isUpdated: false,
  error: null,
}

export function reducer(
  state = intialState,
  action: MediasActionsUnion,
): MediasState {
  switch(action.type) {

    case MediasActionTypes.MediasLoad: {
      return {
        ...state,
        isLoading: true,
      };
    }

    case MediasActionTypes.MediasLoadComplete: {
      return {
        ...state,
        medias: action.payload,
        isLoading: false,
        isLoaded: true,
      };
    }

    case MediasActionTypes.MediasLoadError: {
      return {
        ...state,
        isLoading: false,
        error: action.payload,
      };
    }

    case MediasActionTypes.SelectedMedia: {
      return {
        ...state,
        media: action.payload,
      };
    }

    case MediasActionTypes.UpdateMedia: {
      return {
        ...state,
        // To Simulate the update on the server side
        media: {
          ...state.media,
          primaryTitle: action.payload.primaryTitle ? action.payload.primaryTitle : state.media.primaryTitle,
          startYear: action.payload.startYear ? action.payload.startYear: state.media.startYear,
          genres: action.payload.genres ? action.payload.genres : state.media.genres,
          formattedAdultMedia: state.media.formattedAdultMedia,
        },
        isUpdating: true,
      };
    }

    case MediasActionTypes.UpdateMediaComplete: {
      return {
        ...state,
        // Change the updated media on the store
        medias: state.medias.map((_media) => {
          if (_media.tconst === state.media.tconst) {
            return state.media;
          }
          return _media;
        }),
        isUpdating: false,
        isUpdated: true,
      };
    }

    case MediasActionTypes.UpdateMediaError: {
      return {
        ...state,
        error: action.payload,
      };
    }

    case MediasActionTypes.MediaUpdateQuery: {
      return {
        ...state,
        queryTitle: action.payload,
      };
    }

    case MediasActionTypes.MediaUpdateDateQuery: {
      return {
        ...state,
        queryDate: action.payload,
      };
    }

    case MediasActionTypes.MediasClear: {
      return state;
    }

    default: {
      return state;
    }
  }
}
