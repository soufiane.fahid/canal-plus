import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { RouterStateSerializer, StoreRouterConnectingModule } from '@ngrx/router-store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { appReducer, CustomSerializer, appMetaReducers } from './store/reducers';
import * as effects from './store/effects';

import { JsonConvert, OperationMode, ValueCheckingMode } from 'json2typescript';
import * as coreServices from './services';
import * as coreMiddlewares from './middleware';
import { environment } from '@env/environment.prod';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    StoreModule.forRoot(appReducer, {
      metaReducers: appMetaReducers,
    }),
    StoreRouterConnectingModule.forRoot(),
    EffectsModule.forRoot([
      ...effects.list,
    ]),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
  ],
  declarations: [],
  providers: [
    ...coreServices.services,
    ...coreMiddlewares.middlewares,
    {
      provide: JsonConvert,
      useFactory: () => {
        const jsonConvert = new JsonConvert();
        jsonConvert.operationMode = OperationMode.ENABLE;
        jsonConvert.valueCheckingMode = ValueCheckingMode.ALLOW_NULL;
        jsonConvert.ignorePrimitiveChecks = true;
        return jsonConvert;
      },
    }
  ]
})
export class CoreModule {
  /*make sure CoreModule is imported by one NgModule the AppModule */
  constructor(
    @Optional()
    @SkipSelf()
    parentModule: CoreModule,
  ) {
    if (parentModule) {
      throw new Error('CoreModule is already loaded. Import only the AppModule');
    }
  }

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: [
        {
          provide: RouterStateSerializer,
          //To be imported within the reducer
          useClass: CustomSerializer,
        }
      ]
    }
  }
}
