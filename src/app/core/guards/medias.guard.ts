import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Observable, of } from 'rxjs';
import { filter, switchMap, tap } from 'rxjs/operators';

import { Store } from '@ngrx/store';
import * as fromStore from '../store';

@Injectable()
export class MediasGuard implements CanActivate {

  constructor (private store: Store<fromStore.AppState>) {}

  //Check if the medias are loaded if not dispatch the Action to load it
  canActivate(): Observable<boolean> {
    return this.checkStore().pipe(
      filter(status => status !== 'initial'),
      switchMap(status => of(status === 'success'))
    )
  }

  checkStore(): Observable<string> {
    return this.store.select(fromStore.getMediasLoadStatus).pipe(
      tap((status: string) => {
        if (status === 'initial') {
          this.store.dispatch(new fromStore.MediasLoad());
        }
      })
    )
  }
}
