import { MediasGuard } from './medias.guard';

export const guard: any[] = [
  MediasGuard,
]

export * from './medias.guard';