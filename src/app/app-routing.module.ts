import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import * as FromGuards from '@app/core/guards';


const routes: Routes = [
  {
    path: '',
    canActivate: [FromGuards.MediasGuard],
    loadChildren: 'app/medias/medias.module#MediasModule',
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    preloadingStrategy: PreloadAllModules,
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
