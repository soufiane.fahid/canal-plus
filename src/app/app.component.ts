import { Component, OnInit } from '@angular/core';
import * as fromStore from '@app/core/store';
import { Store, select } from '@ngrx/store';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'canal-plus';
  subscription$: Subscription = new Subscription();

  constructor(
    private store: Store<fromStore.AppState>,
  ) {}

  ngOnInit() {}
}
