# CanalPlus

Pour lancer l'application :
  'npm run start': pour lancer l'application angular
  'npm run server': pour lancer le serveur python

Le porjet s'appuie sur le NGRX pour fournir une application 100% réactive et un serveur node pour renvoyer les données. Il tourne avec sous l'architecture:

API:
  server.js: command pour le lancer 'npm run server'
    serveur node qui traitent les requêtes utilisateurs et renvoie les données
    Améliorations possibles: Traiter les requêtes PUT et modifier les données de data.json

  formatted.py: un script python qui prend le data.tsv et retourne le data.json il n'est pas à 100% opérationnel
    vu qu'il retourne tous les champs dans un seul attribut et ne s'appuie pas sur le séparateur.
    Commande pour l'utiliser 'npm run generator'

  data.json: les données extraites du tsv et utilisées par le server le format de données est bon.

APP/CORE: Contient la partie fonctionnelle de l'application et se divise en ses sous-packages:

  Guards: c'est l'ensemble des Guards qui implémente les règles à respecter avant d'afficher un composant: Dans cette application medias guards qui va vérifier si les medias ont été chargés sinon il va dispatcher une action pour les charger.

  Middleware: C'est l'ensemble des services qui vont formater la donnée récupérée du serveur avant de la mapper sous le model. Dans ce contexte il est utilisé pour vérifier si les médias sans autorisés au mineurs en changant l'input isAdult avec une RG.

  models: Contient le model Media.

  Request: contient les interface utilisées pour contacter le serveur dans l'application c'est utilisée pour l'interface de la modification du média.

  Service: Contient l'ensemble des services utilisées dans l'application dataService pour centraliser les fonctions de 'GET - PUT- POST - DELETE' et le mediaService pour charger les données et les modifier

  Validators: Contient les validateurs customizés utilisés pour valider les formulaires.

  Store: S'appuie sur le paradigme de la programmation fonctionnelle, et se compose de :

    Reducers: Qui contient le reducer de l'application c un json qui change en fonction de l'action dispatchée.

    Actions: Ensemble des actions utilisées pour modifier le store (Reducers) et sur lesquelle les effects écoutent pour appeler les différents services et dispatch d'autres actions à la fin

    Effects: Cette partie se charger d'exécuter les traitements en dehors du contexte de l'application chager les données par exemple.

    Selectors: Utiliser pour récupérer une portion ou l'état du store. Il faut noter que leur output est memorisé du moment que le store ne connait pas de changement. Ils sont utilisés par les composants pour s'enregistrer à leurs flux de données.


APP/Medias: Les différents composants du module Média.

APP/SHARED : Les composants partagés par tous les modules dans l'application est utilisé pour contenir le loader 'spinner'.

ASSETS: pour les différentes ressources statiques.
